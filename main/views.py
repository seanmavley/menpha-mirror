# Views.py Handwritten by google.com/+Nkansahrexford
from django.core.mail import send_mail
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from main.models import Item, Blog
from main.forms import ItemForm, NotifyForm
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
#from django.contrib import messages # Apply later
from django.views.generic import ListView
#from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.core.urlresolvers import reverse_lazy
from main.serializers import ItemSerializer
from rest_framework import generics
from rest_framework.renderers import JSONRenderer


def intro_page(request):
    """Your homepage with search bar"""
    return render(request, 'homepage.html')


#Django Rest Framework Related APIs. All Classbased
class CreateViaAPI(generics.ListCreateAPIView):
    '''The Add page item creation REST api'''
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def pre_save(self, obj):
        obj.created_by = self.request.user


# Error: method post not allowed. Look into. Not in use now
class UpdateViaAPI(generics.UpdateAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class ItemDetailViaAPI(generics.RetrieveAPIView):
    '''Allows JSON access to an Item Details page.'''
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    renderer_classes = (JSONRenderer,)


class ItemMyListViaAPI(generics.ListAPIView):
    '''In JSON, displays all items belonging to person in context'''
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    renderer_classes = (JSONRenderer,)


# Non REST related functions and Class Based View
@login_required
def profile(request):
    '''Displays profile name under your Account'''
    account = request.user.get_username()
    return render(request, 'account.html', {'account': account})


def search(request):
    """ Displays results of the searched query"""
    query = request.GET.get('q', '')
    if query:
        qset = Q(slug__iexact=query, stolen__iexact='s')
        results = Item.objects.filter(qset).distinct()
    else:
        results = []

    return render(request, "search.html", {
        'results': results,
        'query': query,
        'user': request.user,  # For checking permissions in template
    })


def direct_search(request, slug):
    """If someone searches for Item using directly like /search/<slug>"""
    query = Q(slug__iexact=slug, stolen__iexact='s')
    results = Item.objects.filter(query).distinct()
    return render(request, 'search.html', {
        'results': results,
        'user': request.user,
        'query': slug
    })


# Using Django Generic CBViews
class CreateImei(CreateView):
    """ Adds an object to database by a user in context.
    In Menpha v2.0, using REST for adding items."""
    template_name = 'add.html'
    form_class = ItemForm
    model = Item

    def form_valid(self, form):
        #save current user into "created_by" field in models.
        form.instance.created_by = self.request.user
        return super(CreateImei, self).form_valid(form)

    @method_decorator(login_required())
    def dispatch(self, * args, ** kwargs):
        return super(CreateImei, self).dispatch(* args, ** kwargs)


class UpdateImei(UpdateView):
    """ Updates an object in context.

    If request.user is same as created_by value in model, dispatch
    fucntion proceeds. Else PermissionDenied. Useful for preventing
    one user from editing item added by another user. """
    model = Item
    fields = ['device', 'slug', 'type_of_item', 'description', 'stolen']
    template_name = 'item_form.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        pulled = Item.objects.get(slug=kwargs['slug'])
        if pulled.created_by == request.user:
            return super(UpdateImei, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied


class DeleteImei(DeleteView):
    """ Deletes the the context object.
    If request.user is same as created_by value in model,
    dispatch fucntion proceeds. Else PermissionDenied.

    Useful for preventing one user from deleting
    item added by another user. """
    model = Item
    context_object_name = 'to_delete'
    success_url = reverse_lazy('myList')
    template_name = 'item_confirm_delete.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        grab = Item.objects.get(slug=kwargs['slug'])
        if grab.created_by == request.user:  # Check if delete is authorized
            return super(DeleteImei, self).dispatch(request, *args, **kwargs)
        raise PermissionDenied


class ListImei(ListView):
    """Lists all the items added by a user."""
    model = Item
    template_name = 'mylist.html'

    def get_queryset(self):
        qset = Q(created_by=self.request.user)
        return Item.objects.filter(qset).distinct().order_by('-pub_date')

    @method_decorator(login_required())
    def dispatch(self, * args, ** kwargs):
        return super(ListImei, self).dispatch(* args, ** kwargs)


def imei_detail(request, slug):
    """ Still used to display detail of object. Should be replaced
    with Class Based View DetailView class."""
    s = get_object_or_404(Item, slug=slug)
    return render(request, 'imei-detail.html', {'mylist': s, 'user': request.user})


class ListBlog(ListView):
    model = Blog
    template_name = 'blog.html'
    context_object_name = 'list'


class BlogDetail(DetailView):
    slug_field = 'slug'
    slug_url_kwarg = 'slug'
    model = Blog
    context_object_name = 'detail'
    template_name = 'blog_detail.html'


@login_required
def notify(request, slug):
    """ Send notification email to original owner of device who lost it """
    grab = Item.objects.get(slug=slug)
    if request.method == "POST":  # If the form has been submitted via 'post'.
        form = NotifyForm(request.POST)  # A form bound to the POST data
        if form.is_valid():  # All validation rules pass
            # Process the data in form.cleaned_data
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = request.user.email
            cc_myself = form.cleaned_data['cc_myself']
            recipients = [grab.created_by.email]

            if cc_myself:
                recipients.append(sender)

            send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/notify/thanks')  # Redirect after POST
    else:
        form = NotifyForm()  # An unbound form
    return render(request, 'send-mail.html', {'form': form, })


#Dummy page requests for testing purposes and learning
def my_form(request):
    return render(request, 'otherForm.html')


def view_account(request):
    return render(request, 'account/oldsignup.html')


def old_login(request):
    return render(request, 'account/oldlogin.html')


# Error Pages. Referred from Settings.py
def file_not_found_404(request):
    return render(request, '404.html')


def server_error(request):
    return render(request, '500.html')


def perm_denied(request):
    return render(request, '403.html')
