from django.test import TestCase
# from django.core.urlresolvers import resolve
from .models import Item
from django.contrib.auth.models import User


class start():
    def create_user(self):
        self.user_in = User.objects.create_user(
                'khophi',
                'email@email.com',
                'password'
            )
        # create user khophi
        self.user_in.save()

    def create_item(self):
        self.username = User.objects.get(username='khophi')
        self.item = Item(
            device='Google Nexus 6',
            slug='0000000000',
            type_of_item='md',
            description='An awesome phone I bought from Google',
            stolen='s',
            created_by=self.username
        )
        self.item.save()


# Check save and retrieve from DB
class SaveToDBDirect(TestCase):
    def setUp(self):
        begin = start()
        begin.create_user()
        begin.create_item()

    def test_check_user_account(self):
        self.user = User.objects.all()[0]

        self.assertEqual(str(self.user), '[<User: khophi>]')

    def test_check_new_item(self):
        from_db = Item.objects.count()

        self.assertEqual(from_db, 1)


# Check request, save and retrieve from DB works via views
# Non REST

class TestView(TestCase):

    def test_check_login(self):
        request = self.client.post('/admin/', {'username': 'khophi', 'password': 'password'})
        self.assertEqual(request.status_code, 200)

    def test_check_details(self):
        request = self.client.get('/detail/0000000000')
        self.assertEqual(request.status_code, 200)

# Check request, save and retrieve from DB works via views
# REST way

# Check post and get works via browser
# Mr. Selenium comes in

# Searched empty, response "Not searched for anything"

# if not stolen, don't show in results

# Mylist count

# Account login, logout

# get_absolute_urls on models
