from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField


class Item(models.Model):
    """ Main model for the Menpha app. """

    TYPE_OF_ITEM = (
        ('auto', 'Automobile'),
        ('md', 'Mobile Devices'),
        ('com', 'Computers'),
        ('da', 'Domestic Appliance'),
    )

    STOLEN_OPTION = (
        ('ns', 'Its with me'),
        ('s', 'Its stolen/missing'),
    )

    device = models.CharField(max_length=250,)
    slug = models.SlugField(max_length=30, unique=True,)
    type_of_item = models.CharField(max_length=20, choices=TYPE_OF_ITEM)
    description = models.TextField()
    stolen = models.CharField(max_length=2, choices=STOLEN_OPTION)
    # had a photo field. No longer needed
    # photo = models.ImageField(upload_to='devices/', blank=True, null=True)
    # photo = ProcessedImageField(blank=True, null=True, upload_to='devices/',
        # processors=[ResizeToFill(250, 250)], format='JPEG',
        # options={'quality': 80})
    pub_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User)

    def __unicode__(self):
        """ Returns device name """
        return self.device

    def get_absolute_url(self):
        """ For reverse url matching in views """
        return reverse('detail', kwargs={'slug': self.slug})


class Blog(models.Model):
    slug = models.SlugField()
    title = models.CharField(max_length=500)
    body = RichTextField()
    pub_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User)

    def __unicode__(self):
        """ Returns device name """
        return self.title

    def get_absolute_url(self):
        """ For reverse url matching in views """
        return reverse('blog_detail', kwargs={'slug': self.slug})
