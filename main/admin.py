# Admin.py by google.com/+Nkansahrexford
from django.contrib import admin
from main.models import Item, Blog


class ItemAdmin(admin.ModelAdmin):
    list_display = ('device', 'slug', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['slug']
    date_hierarchy = 'pub_date'


class BlogAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Item, ItemAdmin)
admin.site.register(Blog, BlogAdmin)
