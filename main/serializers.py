from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Item


class ItemSerializer(serializers.ModelSerializer):
    created_by = serializers.Field(source='created_by.username')

    class Meta:
        model = Item
        fields = ('id', 'device', 'slug', 'type_of_item', 'description', 'stolen', 'created_by')
